from time import sleep
import pychrome
import json
import subprocess
import pyautogui
from numpy import array as np_array
from skimage.metrics import structural_similarity as ssim
import os
from PIL import Image

def clear_terminal() -> None:
    """Clears the terminal"""
    os.system("cls" if os.name == "nt" else "clear")

def run_chrome() -> None:
    """Opens an instance of Chrome"""
    command = "google-chrome --remote-debugging-port=9222"
    subprocess.Popen(command, shell=True)

def get_resolutions() -> dict[int, int]:
    """Returns all resolutions from the json file"""
    with open("resolutions.json", 'r') as json_file:
        return json.load(json_file)

def enable_emulation(tab: pychrome.Tab, width: int, height: int) -> None:
    """Create a emulation for given width and height"""
    device_metrics = {
        "width": width,
        "height": height,
        "deviceScaleFactor": 0,
        "mobile": False
    }

    # override default device metrics
    tab.Emulation.setDeviceMetricsOverride(**device_metrics)

def submit_form(tab: pychrome.Tab, form_selector: str) -> None:
    """Submit a form based on the given form selector"""
    try:
        # submits the form
        js_code = f"document.querySelector('{form_selector}').submit();"
        tab.Runtime.evaluate(expression=js_code)

        tab.wait(0.5)

    except Exception as error:
        print(f"Error while submitting the form : {error}")

def check_resolution(width: int, height: int, user_agent:str, website_url: str) -> list[tuple()]:
    """Opens chrome with given params and calls other relevant functions to evaluate responsiveness"""
    browser = pychrome.Browser(url="http://127.0.0.1:9222")
    results = []

    try:
        # open new tab
        tab = browser.new_tab()
        tab.start()

        # override default user agent string to custom string
        tab.Network.setUserAgentOverride(userAgent=user_agent)

        # override default device metrics
        enable_emulation(tab, width, height)

        # navigate to new tab
        tab.Page.navigate(url=website_url)

        # waits 0.5 seconds so the page can load
        tab.wait(0.5)

        # create new folder for specified width and height
        os.makedirs(f"{os.path.curdir}/output/w{width}_h{height}", exist_ok=True)

        match_percentage = evaluate_responsiveness(width, height, "imgs/settings.png", "settings")
        results += [(width, height, "settings", match_percentage)]

        submit_form(tab, "#settings_container")

        match_percentage = evaluate_responsiveness(width, height, "imgs/game.png", "game")
        results += [(width, height, "game", match_percentage)]

        # check if certain class is present in the current html
        if check_game_state(tab):
            match_percentage = evaluate_responsiveness(width, height, "imgs/result.png", "result")
            results += [(width, height, "result", match_percentage)]

            # closes the current tab
            tab.Page.close()

        return results

    except Exception as e:
        print(f"Error while opening website: {e}")

    finally:
        # closes the browser window
        browser.close_tab(tab)

def evaluate_responsiveness(width: int, height: int, base_img_path: str, identifier: str) -> float:
    """Evaluates the responsiveness of the current chrome window"""
    screenshot_path = take_screenshot(width, height, identifier)

    return compare_images(base_img_path, screenshot_path, (width, height))

def take_screenshot(width: int, height: int, identifier: str) -> str:
    """Takes a screenshot with the given width and height and saves it"""

    # take the screenshot for specified width and height
    screenshot = pyautogui.screenshot(region=(0, 108, width, height))
    screenshot.save(f"output/w{width}_h{height}/{identifier}_screenshot_w{width}_h{height}.png")

    # return the custom path of the screenshot
    return f"output/w{width}_h{height}/{identifier}_screenshot_w{width}_h{height}.png"

def compare_images(base_img_path: str, screenshot_path: str, target_size: tuple) -> float:
    """Compares two images and returns a float indicating the percentage match"""

    # converts images to grayscale
    base_img = Image.open(base_img_path).convert('L')
    screenshot_img = Image.open(screenshot_path).convert('L')

    # resizes the images to match sizes
    base_img_resized = base_img.resize(target_size, Image.LANCZOS)
    screenshot_img_resized = screenshot_img.resize(target_size, Image.LANCZOS)

    # converts PIL images to numpy arrays
    base_img_array = np_array(base_img_resized)
    screenshot_img_array = np_array(screenshot_img_resized)

    # calculates the similarity between the images
    similarity_score, _ = ssim(base_img_array, screenshot_img_array, full=True)

    # converts the similarity score to a percentage between 0.0 and 100.0
    similarity_score = similarity_score * 100.0

    return similarity_score

def check_game_state(tab: pychrome.Tab) -> bool:
    """Check if a class exists based on the class name"""
    while True:
        js_code = f"document.querySelector('.replay_btn') !== null;"
        result = tab.Runtime.evaluate(expression=js_code)
        sleep(1)

        if result["result"]["value"]:
            break

    return True

def display_results(all_results: list[dict], average_identifier: bool) -> None:
    """Displays the results in the terminal and checks match percentage for adding colors"""

    # ascii color codes
    red = "\033[91m"
    light_blue = "\033[94m"
    orange = "\033[38;5;208m"
    green = "\033[92m"
    reset_color = "\033[0m"

    # changes color based on the match percentage
    for results in all_results:
        if results["match_percentage"] == 100:
            color = light_blue
        elif 65 <= results["match_percentage"] <= 100:
           color = green
        elif 60 <= results["match_percentage"] <= 65:
            color = orange
        else:
            color = red

        # print line for the average match percentage results
        if average_identifier:
            print(f"{color}{results['identifier']:8} matched about {results['match_percentage']:.2f}% on average.{reset_color}")
            continue
        
        # print line for all match percentage results
        print(f"{color}{results['identifier']:8} width : {results['width']} height : {results['height']} matched about {results['match_percentage']:.2f}%{reset_color}")

def get_average_results(all_results: list[dict]) -> None:
    """Get average result for all identifiers"""

    # set the order for the loop
    order = {"settings": 1, "game": 2, "result": 3}
    average_results = []

    # sort the results based on the identifier
    sorted_results = sorted(all_results, key=lambda item: order.get(item["identifier"], float("inf")))

    for key in order:
        total = 0

        for result in sorted_results:
            if key == result["identifier"]:
                total += result["match_percentage"]

        # get the number of items with the same identifier
        total_items = len(all_results) / 3

        # calculates the average match percentage and adds it to the result list
        average_results.append({"identifier": key, "match_percentage": round(total/total_items, 2)})
    
    # calls display result function, but with average match percentages
    display_results(average_results, True)
    
def main() -> None:
    run_chrome()
    resolutions = get_resolutions()
    sleep(1)
   
    user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36"
    website_url = "http://localhost:5313"

    # creates output dir or overwrites existing output dir
    os.makedirs(f"{os.path.curdir}/output", exist_ok=True)
    all_results = []

    for resolution in resolutions:
        results = check_resolution(resolution["width"], resolution["height"], user_agent, website_url)

        for result in results:
            # format data into dict and add it to the result list
            all_results.append({"width": result[0], "height": result[1], "identifier": result[2], "match_percentage": result[3]})

        # sorts the results list based on match percentage
        all_results = sorted(all_results, key=lambda x: x["match_percentage"])

        clear_terminal()

        print("Average match results : \n")
        get_average_results(all_results)
        print("\n\nAll match results : \n")
        display_results(all_results, False)

        sleep(1)

if __name__ == "__main__":
    main()